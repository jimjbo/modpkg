# ModPKG

ModPKG is a package manager for UNIX-like systems focused on being simple, powerful and in the hands of it's users.

## Contents

1. [Introduction](#introduction)
2. [Feature Set](#feature-set)
3. [Philosophy](#philosophy)
4. [Inspirations](#inspirations)

## Introduction

The motivation for this project is to build a package manager with as few compromises as possible, be they in performance, philosophy or anything else. It's goal is to eventually serve as the package manager of its own Linux distribution with similar principles, of which there seems to be a lack of. For the majority of distributions their sole differentiators are their repositories and their release models. That is where ModPKG will stand as software focused on user choice and freedom, so you can build your system how you want to, rather than how a board of directors wants.

## Feature Set

* Modularity - Package Managers should never limit you on your software, it's there to install software not restrict it. If there's a better tool for the job you should be able to use it.
* Atomic Upgrades - All packages are managed through symbolic links, which are simply moved when new versions of packages are installed. If any breakages occurred, the link can be simply reversed, pointing to the older package. When these packages accrue disk space, all package versions except the selected will be wiped.
* Package Categorization - Isolate and organize your packages into "decks". A deck can be enabled and disabled as you see fit, allowing your system to be under your full control.

## Philosophy

* Choice - Never settle, choose what you want to use and when you want to use it. This is your system.
* Readable and Refined Codebase - Code should always be accessible to those who wish to explore it.
* Independent - The project can be seperated from any entity, with interchangable parts, with little to no friction. The only person it depends on is it's user.

### Why the POSIX shell?

The shell is universal amongst unix operating systems, and by adhering to the POSIX standard, maintaining true portability is a much simpler task. In addition, the shell allows much flexibility in the tools that can be used with your system. Don't want to use the GNU utils? You don't have to! What software you install is up to you.

## Inspirations

* [Pacman](https://www.archlinux.org/pacman/) - The Package Manager for Arch, simplistic fast and powerful, suitably verbose for resolving problems and breakages.
* [Portage](https://wiki.gentoo.org/wiki/Portage) - Gentoo's Package Manager features many customisation utilities, giving the power to it's users.
* [Nix](https://nixos.org/) - The Nix Package Manager, portable and reproducible, with it's unique ability to do atomic upgrades.
* [Suckless](https://suckless.org/) - The general idea for Suckless Philosophy is key to maintaining secure and simple code.
